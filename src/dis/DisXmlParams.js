// @ts-check
const
  _ = require('lodash'),
  DisXmlDataSet = require('./DisXmlDataSet'),
  XmlData = require('../XmlData'),
  DisXmlCmd = require('./DisXmlCmd'),
  xml = require('../xml');

/**
 * @typedef {import('../types').XmlParamPart} XmlParamPart
 * @typedef {import('../types').XmlDataSet} XmlDataSet
 * @typedef {import('./types').DisXmlCmd.ReqInfo} ReqInfo
 * @typedef {import('./types').DisXmlCmd.Reply} Reply
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 */

/**
 * @param  {Element} data
 * @return {XmlParamPart}
 */
function parseData(data) {
  if (_.isEmpty(data.getAttribute('type')) &&
    _.isEmpty(data.getAttribute('value'))) {
    return {
      value: _.map(xml.getChildNodes(data, 'data'), parseData),
      index: _.toNumber(data.getAttribute('index'))
    };
  }
  else {
    return {
      index: _.toNumber(data.getAttribute('index')),
      type: _.toNumber(data.getAttribute('type')),
      value: data.getAttribute('value') || ''
    };
  }

}

class DisXmlParams extends DisXmlDataSet {
  constructor() {
    super();
    this.cmd = new DisXmlCmd(this._handle.bind(this));
    this._tag = 'parameters';
  }

  /**
   * @param  {XmlDataSet} dataset
   * @param {XmlParamPart} data
   * @return {boolean}
   */
  _checkParam(dataset, data) {
    const index = _.get(data, 'index');
    if (!data || index < 0) { return false; }

    const param = _.find(dataset, { index });
    if (!param) { return false; }

    if (_.isArray(data.value)) {
      let ret = true;
      _.forEach(data.value, (nestedValue) => {
        ret = this._checkParam(param.value, nestedValue) && ret;
      });
      return ret;
    }
    else {
      return !_.isNil(data.value) &&
        this._isSameType(param, data.value, data.type);
    }
  }

  /**
   * @brief check incoming parameters
   * @param {XmlParamPart} data Element to check
   * @return {boolean}
   *
   * @details override to make custom typechecks
   */
  checkParam(data) {
    return this._checkParam(this.data, data);
  }

  /**
   * @param  {DicReqInfo} req
   * @return {void}
   */
  _handle(req) {
    if (!req || !(/** @type {ReqInfo} */ (req).xml)) {
      throw 'invalid request: xml not found';
    }

    var elt = xml.getChildNodes(
      /** @type {ReqInfo} */ (req).xml, 'parameters')[0];
    if (!elt) { throw 'invalid request: missing parameters'; }

    var parameters = _.map(xml.getChildNodes(elt, 'data'), parseData);
    var fail = _.find(parameters,
      (data) => !this.checkParam(data));
    if (fail) {
      throw 'invalid request: failed to validate attr:' +
        _.toString(fail.index);
    }

    _.forEach(parameters,
      (param) => {
        // @ts-ignore
        param.value = XmlData.parseValue(param.value, param.type);
        this.update(param);
      });
  }
}

DisXmlParams.parseData = parseData;

module.exports = DisXmlParams;
