// @ts-check
const
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisXmlNode, DicXmlCmd, xml, XmlCmd } = require('../src');

describe('DicXmlCmd', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can make a call', function() {
    var cmd = env.dis.addXmlCmd('/pwet', _.noop);
    if (!cmd) { throw 'should have a cmd'; }

    return DicXmlCmd.invoke('/pwet', { test: 42 }, env.dis.info, 666)
    .then((ret) => expect(ret).to.deep.contain(
      { error: 0, key: 666, status: 2 }));
  });

  it('can send and retrieve information', function() {
    env.dis.addXmlCmd('/pwet', (req, ret) => {
      expect(req.xml).to.be.ok();
      var params = xml.toJs(req.xml);
      expect(params).to.deep.contains({ pwet: '42' });
      xml.fromJs(ret, params);
    });

    var cmd = new DicXmlCmd('/pwet', env.dis.info);
    return cmd.invoke({ pwet: 42 }, 1234)
    .then((rep) => {
      var params = xml.toJs(rep.xml);
      expect(params).to.deep.contains({ pwet: '42' });
    })
    .catch((rep) => {
      var params = xml.toJs(rep.xml);
      expect(params).to.deep.contains({ pwet: '42' });
    })
    .finally(() => cmd.release());
  });

  it('can handle errors', function() {
    env.dis.addXmlCmd('/pwet', () => {
      throw XmlCmd.createError(12, 'sample');
    });

    return DicXmlCmd.invoke('/pwet', { test: 42 }, env.dis.info)
    .then(
      () => expect.fail('should fail'),
      (ret) => {
        expect(ret).to.contain(
          { error: 12, status: XmlCmd.Status.ERROR, message: 'sample' });
      }
    );
  });
});
