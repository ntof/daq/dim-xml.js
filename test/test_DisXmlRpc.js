// @ts-check
const
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisService, DisCmd } = require('@cern/dim'),

  { DisXmlNode } = require('../src');

describe('DisXmlRpc', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can register a rpc service', function() {
    var cmd = env.dis.addXmlRpc('/pwet', _.noop);

    if (!cmd) { throw 'should have a cmd'; }

    expect(cmd.rpc.rpcOut).to.be.instanceof(DisService);
    expect(cmd.rpc.rpcIn).to.be.instanceof(DisCmd);
    expect(env.dis.isService('/pwet/RpcIn')).to.be.true();
    expect(env.dis.isService('/pwet/RpcOut')).to.be.true();
  });
});
