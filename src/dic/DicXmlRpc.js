// @ts-check
const
  DicXmlCmd = require('./DicXmlCmd');

/**
 * @typedef {import('@cern/dim').ServiceInfo} ServiceInfo
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@cern/dim').NodeInfo} NodeInfo
 * @typedef {import('./types').DicXmlCmd.Reply} Reply
 */

class DicXmlRpc extends DicXmlCmd {
  /**
   * @param {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)?=} dns
   */
  _init(service, dns) {
    super._init(service, dns, true);
  }

  /**
   * @param {string} service
   * @param {any} args
   * @param {(DnsClient|NodeInfo|string)?=} dns
   * @param {number?=} key
   * @param {number?=} timeout
   * @return {Q.Promise<Reply>}
   */
  static invoke(service, args, dns, key, timeout) {
    var rpc = new DicXmlRpc(service, dns);
    return rpc.invoke(args, key, timeout)
    .finally(rpc.release.bind(rpc));
  }
}

module.exports = DicXmlRpc;
