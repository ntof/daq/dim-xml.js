
import * as dim from '@cern/dim'

export = dimXml;
export as namespace dimXml;

declare namespace dimXml {
  export class DicXmlCmd extends dim.DicService {
      static invoke(service: string, args: any, dns?: (dim.DnsClient | dim.NodeInfo | string | null), key?: number | null, timeout?: number | null): Promise<any>;
      constructor(service: string | dim.ServiceInfo, dns?: (string | dim.DnsClient | dim.NodeInfo | null));
      ackSid: number | undefined;
      invoke(args: any, key?: (number | null) | undefined, timeout?: (number | null) | undefined): Promise<DicXmlCmd.Reply>;
  }
  export namespace DicXmlCmd {
    export const TIMEOUT: number;
    export interface Reply {
      key: number,
      status: XmlCmd.Status,
      error: number,
      xml: Element,
      message?: string | null
    }
  }

  export class DicXmlDataSet extends dim.DicValue<XmlDataSet> {
      static parse(xmlsource: string, parser?: (DOMParser | null)): XmlDataSet | null;
      constructor(service: string | dim.ServiceInfo, options?: ({
          timeout?: number;
          stamped?: boolean;
      } | null) | undefined,
      dns?: (dim.DnsClient | dim.NodeInfo | string | null));
  }

  export class DicXmlParams extends DicXmlDataSet {
      constructor(service: string, options?: ({
          timeout?: number;
          stamped?: boolean;
      } | null) | undefined,
      dns?: ((dim.DnsClient | dim.NodeInfo | string) | null) | undefined);

      cmd: DicXmlCmd;
      setParams(params: Array<DicXmlParams.XmlParamPart> | DicXmlParams.XmlParamPart): Promise<DicXmlCmd.Reply>;
  }
  export namespace DicXmlParams {
      interface XmlParamPart {
          value: string | number | boolean | Array<XmlParamPart>;
          type?: number | undefined | null;
          index: number;
      }
  }

  export class DicXmlRpc extends DicXmlCmd {
      constructor(service: string | dim.ServiceInfo, dns?: string | dim.DnsClient | dim.NodeInfo | null | undefined);
      static invoke(service: string, args: any,
        dns?: ((dim.DnsClient | dim.NodeInfo | string) | null) | undefined,
        key?: (number | null) | undefined,
        timeout?: (number | null) | undefined): Promise<DicXmlCmd.Reply>;
  }

  export class DicXmlState extends dim.DicValue<XmlState> {
      constructor(service: string | dim.ServiceInfo, options?: ({
          timeout?: number;
          stamped?: boolean;
      } | null),
      dns?: (dim.DnsClient | dim.NodeInfo | string | null));

      static parse(xmlsource: string, parser?: (DOMParser | null) | undefined): XmlState | null;
      static get<T=XmlState>(service: string | dim.ServiceInfo, dns: (dim.DnsClient | dim.NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<T>;
  }

  export interface XmlState extends XmlState.State {
    errors: Array<XmlState.Error>,
    warnings: Array<XmlState.Error>,
    description: Array<XmlState.State>
  }

  export namespace XmlState {
    export interface Error {
      code: number,
      message: string
    }

    export interface State {
      value: number,
      strValue: string
    }

    export namespace States {
      export const NotReady: State;
      export const Error: State;
    }
  }

  export class DicXmlValue extends dim.DicValue<Element> {
    constructor(service: string | dim.ServiceInfo, options?: ({
        timeout?: number;
        stamped?: boolean;
    } | null) | undefined, dns?: ((dim.DnsClient | dim.NodeInfo | string) | null) | undefined);
    static parse(xmlsource: string | Buffer, parser?: (DOMParser | null) | undefined): Element | null;
    static get<T=Element>(service: string | dim.ServiceInfo, dns: (dim.DnsClient | dim.NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<T>;
  }

  export class DisXmlCmd {
    constructor(callback: (req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply | void);
    value: (req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply | void;
    cmd: dim.DisCmd<DisXmlCmd.ReqInfo>;
    ack: dim.DisService<any>;
    handle(req: dim.DicReqInfo<any>): Promise<void>;
  }

  export namespace DisXmlCmd {
    export interface Reply {
      key?: number,
      status: XmlCmd.Status,
      error: number,
      message?: string | null
    }

    export interface ReqInfo extends dim.DicReqInfo {
      xml: Element
    }
  }

  export namespace XmlCmd {
    export enum Status {
      OK = 2,
      ERROR = 3
    }

    export function createError(error: number, message: string): DisXmlCmd.Reply
  }

  export class DisXmlDataSet extends dim.DisService<string> {
    data: XmlDataSet;

    add(data: DisXmlDataSet.XmlDataInPart): number;
    update(index: number, value: any, override?: boolean): boolean;
    update(part: DisXmlDataSet.UpdatePart, override?: boolean): boolean;
    get(index: number): (number | string | XmlDataSet | undefined);
    getNested(...path: Array<number>): XmlDataSet | undefined;
    remove(...path: Array<number>): boolean;
  }
  export namespace DisXmlDataSet {
    export interface XmlDataInPart {
      name?: string,
      index?: number,
      unit?: string,
      type?: XmlData.Type,
      value: any,
      valueName?: string,

      enum?: XmlEnumDesc
    }

    export interface UpdatePart {
      index: number,
      value: number|string|Array<UpdatePart>|boolean,
      type?: XmlData.Type
    }
  }

  export interface XmlData {
    name?: string,
    index: number,
    unit?: string,
    type?: XmlData.Type,
    value: any,
    valueName?: string,

    enum?: XmlEnumDesc
  }
  export namespace XmlData {
    export function parseValue(value: any, type: number): string | number | boolean;
    export enum Type {
      INVALID = -1,
      NESTED = 0,
      INT32 = 1,
      DOUBLE = 2,
      STRING = 3,
      INT64 = 4,
      ENUM = 5,
      INT8 = 6,
      INT16 = 7,
      FLOAT = 8,
      BOOL = 9,
      UINT32 = 11,
      UINT64 = 14,
      UINT8 = 16,
      UINT16 = 17
    }
  }
  export type XmlDataSet = Array<XmlData>
  export type XmlEnumDesc = Array<{ name: string, value: number }>

  export class DisXmlNode extends dim.DisNode {
    constructor(addr?: string | null | undefined, port?: number | null | undefined, nodeInfo?: dim.NodeInfo | null | undefined, ...args: any[]);
    addXmlRpc(name: string, cb: DisXmlRpc | ((req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply|void)): DisXmlRpc | null;
    removeXmlRpc(name: string): void;
    addXmlCmd(name: string, cb: DisXmlCmd | ((req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply|void)): DisXmlCmd | null;
    removeXmlCmd(name: string): void;
    addXmlDataSet(name: string, dataset?: (DisXmlDataSet | null) | undefined): DisXmlDataSet | null;
    removeXmlDataSet(name: string): void;
    addXmlState(name: string, state?: (DisXmlState | null) | undefined): DisXmlState | null;
    removeXmlState(name: string): void;
    addXmlParams(name: string, params?: (DisXmlParams | null) | undefined): DisXmlParams | null;
    removeXmlParams(name: string): void;
  }

  export class DisXmlParams extends DisXmlDataSet {
    constructor();
    checkParam(data: DisXmlParams.XmlParamPart): boolean;
    cmd: DisXmlCmd;
  }
  export namespace DisXmlParams {
    export interface XmlParamPart {
      value: string | number | boolean | XmlParamPart[];
      type?: number | undefined;
      index: number;
    }
    export function parseData(data: Element): XmlParamPart;
  }

  export class DisXmlRpc {
    constructor(callback: (req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply | void);
    value: (req: DisXmlCmd.ReqInfo, ret: Element) => DisXmlCmd.Reply | void;
    rpc: dim.DisRpc<any, Promise<void>>;
    handle(req: dim.DicReqInfo<any>): Promise<void>;
  }

  export class DisXmlState extends dim.DisService<string> {
    states: XmlState;
    addState(value: number, strValue: string): boolean;
    removeState(value: number): boolean;
    setState(value: number): boolean;
    addWarning(code: number, message: string): void;
    removeWarning(code: number): boolean;
    addError(code: number, message: string): void;
    removeError(code: number): boolean;
  }

  export namespace xml {
    export type DOMParser = typeof DOMParser

    export function createDocument(tag: string): Document;
    export function serializeToString(node: Node): string;
    export function fromJs(node: Element, args: xml.XmlJsObject): Element;
    export function toJs(node: Element): xml.XmlJsObject;
    export function getText(node: Element): string;
    export function getChildNodes(node: Node | Element, tag: string): Array<Element>;

    export interface XmlJsAttrList { [attrName:string]: any }

    export interface XmlJsObject {
      '$'?: XmlJsAttrList
      '$textContent'?: string
      [tagName:string]: Array<XmlJsObject> | XmlJsObject | any | string
    }
  }
}
