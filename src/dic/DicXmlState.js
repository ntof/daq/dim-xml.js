// @ts-check
const
  _ = require('lodash'),
  { DicValue } = require('@cern/dim'),
  DicXmlValue = require('./DicXmlValue'),

  xml = require('../xml');

/**
 * @typedef {import('@cern/dim').ServiceInfo} ServiceInfo
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@cern/dim').NodeInfo} NodeInfo
 * @typedef {import('../types').XmlState} XmlState
 */

/**
 * @param  {Element} error
 */
function parseErr(error) {
  return {
    code: _.toNumber(error.getAttribute('code')),
    message: xml.getText(error)
  };
}

/**
 * @param  {Element} value
 */
function parseValue(value) {
  return {
    value: _.toNumber(value.getAttribute('value')),
    strValue: value.getAttribute('strValue') || ''
  };
}

/**
 * @extends DicValue<XmlState>
 */
class DicXmlState extends DicValue {
  /**
   * @param {string|ServiceInfo} service
   * @param {{ timeout?: number, stamped?: boolean }?=} options
   * @param {(DnsClient|NodeInfo|string)?=} dns
   */
  constructor(service, options, dns) {
    super(service, options, dns);
    this._parser = new xml.DOMParser();
  }

  /**
   * @param {any} rep
   */
  _setValue(rep) {
    if (rep && rep.data) {
      rep.value = DicXmlState.parse(rep.data.toString(), this._parser);
    }
    // @ts-ignore internal API
    super._setValue(rep);
  }

  /**
   * @param {string} xmlsource
   * @param {?DOMParser=} parser
   * @return {?XmlState}
   */
  static parse(xmlsource, parser) {
    var root = DicXmlValue.parse(xmlsource, parser);
    if (!root) {
      return null;
    }

    let warnings = xml.getChildNodes(root, 'warnings');
    warnings = xml.getChildNodes(warnings[0], 'warning');
    let errors = xml.getChildNodes(root, 'errors');
    errors = xml.getChildNodes(errors[0], 'error');

    return {
      value: _.toNumber(root.getAttribute('value')),
      strValue: root.getAttribute('strValue') || '',
      errors: _.map(errors, parseErr),
      warnings: _.map(warnings, parseErr),
      description: _.map(xml.getChildNodes(root, 'value'), parseValue)
    };
  }

  /**
   * @template T=XmlState
   * @param {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)?} dns
   * @param {number?=} timeout
   * @return {Promise<T>}
   */
  static async get(service, dns, timeout) {
    const rep = await DicValue.get(service, dns, timeout);
    var ret = DicXmlState.parse(rep || '');
    if (_.isNil(ret)) {
      throw new Error('failed to parse document');
    }
    // @ts-ignore
    return ret;
  }
}

module.exports = DicXmlState;
