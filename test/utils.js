// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('test');

function waitEvent(obj /*: events$EventEmitter */,
                   evt /*: string */,
                   count /*: ?number */,
                   timeout /*: ?number */) /*: q$Promise<any> */ {
  var def = q.defer();
  var ret = [];
  var received = 0;

  var stack = (new Error()).stack;

  count = _.defaultTo(count, 1);
  timeout = _.defaultTo(timeout, 1000);

  var cb = function(e) {
    ret.push(e);
    debug('waitEvent: received:%s', evt);
    /* $FlowIgnore */
    if (++received >= count) { def.resolve((count === 1) ? ret[0] : ret); }
  };
  obj.on(evt, cb);
  return def.promise
  .timeout(timeout)
  .catch((err) => {
    err.stack = stack;
    throw err;
  })
  .finally(function() {
    if (def.promise.isPending()) { def.reject(); }
    obj.removeListener(evt, cb);
  });
}


module.exports = {
  waitEvent
};
