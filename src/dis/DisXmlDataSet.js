// @ts-check

const
  prom = require('@cern/prom'),
  _ = require('lodash'),
  XmlData = require('../XmlData'),
  xml = require('../xml'),
  { DisService } = require('@cern/dim');


/**
 * @typedef {import('../types').XmlEnumDesc} XmlEnumDesc
 * @typedef {import('../types').XmlDataSet} XmlDataSet
 * @typedef {import('../types').XmlData} XmlData
 * @typedef {import('../types').XmlDataType} XmlDataType
 *
 * @typedef {import('./types').XmlDataInPart} XmlDataInPart
 * @typedef {import('./types').DisXmlDataSet.UpdatePart} UpdatePart
 */

/**
 * @param  {Partial<XmlData>}  data
 * @return {boolean}
 */
function isNestedData(data) {
  return (_.isNil(data.type) || (data.type === XmlData.Type.NESTED));
}

/**
 * @param {Element} elt
 * @param {XmlEnumDesc} desc
 * @return {Element}
 */
function serializeEnum(elt, desc) {
  _.forEach(desc, function(d) {
    var value = elt.ownerDocument.createElement('value');
    value.setAttribute('name', d.name);
    value.setAttribute('value', _.toString(d.value));
    elt.appendChild(value);
  });
  return elt;
}

/**
 * @param  {string} tag
 * @param  {XmlDataSet} data
 * @return {Element}
 */
function serializeData(tag, data) {
  const root = xml.createDocument(tag).documentElement;
  // eslint-disable-next-line complexity
  _.forEach(data, function(dataItem) {
    /** @type {Element} */
    var elt = root.ownerDocument.createElement('data');
    // Checked for nested data
    if (isNestedData(dataItem)) {
      elt = serializeData('data', dataItem.value);
      elt.setAttribute('index', _.toString(dataItem.index));
      if (dataItem.name && !_.isEmpty(dataItem.name)) {
        elt.setAttribute('name', dataItem.name);
      }
    }
    else {
      if (dataItem.name && !_.isEmpty(dataItem.name)) {
        elt.setAttribute('name', dataItem.name);
      }
      if (dataItem.unit && !_.isEmpty(dataItem.unit)) {
        elt.setAttribute('unit', dataItem.unit);
      }
      elt.setAttribute('index', _.toString(dataItem.index));
      elt.setAttribute('type', _.toString(dataItem.type));
      let value = dataItem.value;
      if (dataItem.type === XmlData.Type.BOOL) {
        value = dataItem.value ? 1 : 0;
      }
      elt.setAttribute('value', _.toString(value));

      if (_.has(dataItem, 'valueName')) {
        elt.setAttribute('valueName', _.toString(dataItem.valueName));
      }
      if (dataItem.type === XmlData.Type.ENUM && dataItem.enum) {
        var e = elt.ownerDocument.createElement('enum');
        serializeEnum(e, dataItem.enum);
        elt.appendChild(e);
      }
    }
    root.appendChild(elt);
  });
  return root;
}

/**
 * @extends DisService<string>
 */
class DisXmlDataSet extends DisService {

  constructor() {
    super('C');
    /** @type {XmlDataSet} */
    this.data = [];
    this._tag = 'dataset';
  }

  /**
   * @param  {?XmlDataSet=} dataset
   * @return {number}
   */
  _nextIndex(dataset) {
    return _.toNumber(_.get(_.last(dataset), 'index', -1)) + 1;
  }

  /**
   * @param {XmlDataSet} dataset
   * @param {XmlDataInPart} data
   * @return {number}
   */
  _add(dataset, data) {
    if (_.isNil(data.index)) {
      data.index = this._nextIndex(dataset);
    }
    const index = data.index;

    var eltIdx = _.findIndex(dataset, { index });
    if (eltIdx >= 0) {
      dataset[eltIdx] = /** @type {XmlData} */ (data);
    }
    else {
      dataset.push(/** @type {XmlData} */ (data));
    }

    if (isNestedData(data)) {
      // Recalculate whole values
      const valueClone = _.clone(data.value);
      data.value = [];
      _.forEach(valueClone, (nestedData) => {
        this._add(data.value, nestedData);
      });
    }
    dataset.sort((e1, e2) => (e1.index - e2.index));
    return index; // return only first element index
  }

  /**
   * @brief update or add data in set
   * @param {XmlDataInPart} data
   * @return {number} data index
   */
  add(data) {
    const index = this._add(this.data, data);
    if (!this._dirty) {
      this._dirty = true;
      _.defer(this._updateXML.bind(this));
    }
    return index;
  }

  /**
   * @param {XmlData} dataset
   * @param {number|string|Array<UpdatePart>|boolean} value
   * @param {XmlDataType?=} type
   * @return {boolean}
   */
  _set(dataset, value, type = undefined) {
    let ret  = false;
    if (dataset.value !== value) {
      dataset.value = value;
      ret = true;
    }
    if (!_.isNil(type) && type !== dataset.type) {
      /* $FlowIgnore: checked above that is not null */
      dataset.type = type;
      ret = true;
    }
    if (dataset.type === XmlData.Type.ENUM) {
      dataset.valueName =
        _.get(_.find(dataset.enum, { value }), 'name', 'unknown');
    }
    return ret;
  }

  /**
   * @param  {XmlData}  elt
   * @param  {number|string|Array<any>|boolean}  value
   * @param  {XmlDataType?=}  type
   * @return {boolean}
   */
  _isSameType(elt, value, type) {
    if (!_.isNil(type)) {
      return elt.type === type;
    }
    return typeof elt.value === typeof value;
  }

  /**
   * @param  {XmlDataSet} dataset
   * @param  {UpdatePart} part
   * @param  {boolean?=} [override=false]
   * @return {boolean}
   */
  _update(dataset, part, override = false) {
    var eltIdx = _.findIndex(dataset, { index: part.index });
    if (eltIdx < 0) {
      // TODO: should override here?
      return false;
    }
    var elt = dataset[eltIdx];

    if (isNestedData(elt)) {
      // Nested Case
      if (_.isArray(part.value)) {
        // We go deep.
        let mod = false;
        _.forEach(part.value, (nValue) => {
          if (!_.isNil(nValue.index)) { // Ignored if index is not provided
            mod = this._update(elt.value, nValue, override) || mod;
          }
        });
        return mod;
      }
      else {
        if (!override) { return false; }
        return this._set(elt, part.value, part.type);
      }
    }
    else {
      // Normal Case
      if (this._isSameType(elt, part.value, part.type) || override) {
        return this._set(elt, part.value, part.type);
      }
      return false;
    }
  }

  /**
   * @param  {Array<any>} args
   * @return {boolean}
   * @details
   * 2 Ways to use this API
   * 1) update(index, value, ?override) -> backward compatibility
   * 2) update(part, ?override)
   * where part = Array<UpdatePart> | UpdatePart
   */
  update(...args) { // eslint-disable-line complexity
    var value;
    var override = false;
    let ret = false;
    var firstParam = args.shift();
    if (_.isNumber(firstParam)) {
      // backward compatibility way
      value = args.shift();
      if (_.isNil(value)) { return false; }
      value = { index: firstParam, value };
      override = args.shift() || false;
      ret = this._update(this.data, value, override);
    }
    else if (_.isArray(firstParam)) {
      // New way
      override = args.shift() || false;
      _.forEach(firstParam, (part) => {
        ret = this._update(this.data, part, override) || ret;
      });
    }
    else if (_.isObject(firstParam)) {
      override = args.shift() || false;
      // @ts-ignore
      ret = this._update(this.data, firstParam, override);
    }
    if (ret && !this._dirty) {
      this._dirty = true;
      _.defer(this._updateXML.bind(this));
    }
    return ret;
  }

  /**
   * @param  {number} index
   * @return {(number|string|XmlDataSet|undefined)}
   */
  get(index) {
    const eltIdx = _.findIndex(this.data, { index });
    if (eltIdx < 0) { return undefined; }
    return this.data[eltIdx].value;
  }

  /**
   * @param  {Array<number>} path
   * @return {XmlDataSet|undefined}
   */
  getNested(...path) {
    let dataset = this.data;
    for (const iPath of path) {
      const eltIdx = _.findIndex(dataset, { index: iPath });
      if (eltIdx < 0) { return undefined; }
      if (!isNestedData(dataset[eltIdx])) { return undefined; }
      dataset = dataset[eltIdx].value;
    }
    return dataset;
  }

  /**
   * @param  {Array<number>} path
   * @return {boolean}
   */
  remove(...path) {
    let dataset = this.data;
    let eltIdx = -1;
    for (const iPath of path) {
      if (eltIdx >= 0) {
        if (!isNestedData(dataset[eltIdx])) { return false; }
        dataset = dataset[eltIdx].value;
      }
      eltIdx = _.findIndex(dataset, { index: iPath });
      if (eltIdx < 0) { return false; }
    }
    if (eltIdx >= 0) {
      dataset.splice(eltIdx, 1);
      return true;
    }
    return false;
  }

  _updateXML() {
    if (this._dirty) {
      this._dirty = false;
      var value = xml.serializeToString(serializeData(this._tag, this.data));
      this.setValue(value);
    }
  }

  get synced() {
    if (!this._dirty) {
      return Promise.resolve();
    }
    else {
      const ret = prom.makeDeferred();
      this.once('update', () => ret.resolve());
      return prom.timeout(ret, 3000);
    }
  }
}

module.exports = DisXmlDataSet;
