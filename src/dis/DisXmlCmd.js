// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  debug = require('debug')('dim:server:xml'),
  xml = require('../xml'),
  XmlCmd = require('../XmlCmd'),
  { DisCmd, DisService } = require('@cern/dim');

/**
 * @typedef {import('./types').DisXmlCmd.Reply} Reply
 * @typedef {import('./types').DisXmlCmd.ReqInfo} ReqInfo
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 */

class DisXmlCmd {
  /**
   * @param {(req: ReqInfo, ret: Element) => Reply|void} callback
   */
  constructor(callback) {
    this.value = callback;
    /** @type {DisCmd<ReqInfo>} */
    this.cmd = new DisCmd('C', this.handle.bind(this));
    this.ack = new DisService('C');
    this._parser = new xml.DOMParser();
  }

  /**
   * @param  {DicReqInfo} req
   * @return {Q.Promise<void>}
   */
  handle(req) {
    if (req && req.value) {
      // promoting to ReqInfo
      /** @type {ReqInfo} */ (req).xml =
        this._parser.parseFromString(req.value).documentElement;
      var root = xml.createDocument('acknowledge').documentElement;
      root.setAttribute('key',
        /** @type {ReqInfo} */ (req).xml.getAttribute('key') || '');

      return q()
      .then(() => _.invoke(this, 'value', req, root))
      .then(
        (ret) => {
          root.setAttribute('status', _.toString(XmlCmd.Status.OK));
          root.setAttribute('error', _.toString(XmlCmd.ERROR_NONE));
          if (ret) {
            root.appendChild(root.ownerDocument.createTextNode(
              _.toString(ret)));
          }
        },
        (ret) => {
          debug('cmd failed:', ret);
          root.setAttribute('status',
            _.get(ret, 'status', XmlCmd.Status.ERROR));
          root.setAttribute('error', _.get(ret, 'error', -1));
          root.appendChild(root.ownerDocument.createTextNode(
            _.get(ret, 'message', _.toString(ret))));
        }
      )
      .finally(() => this.ack.emit('update', xml.serializeToString(root)));
    }
    else {
      debug('failed to parse command:', req.value);
      const root = xml.createDocument('acknowledge').documentElement;
      root.setAttribute('key', "-1"); // Error mode when we can't parse key
      xml.fromJs(root,
        { "$": XmlCmd.createError(-1, 'failed to parse command') });
      this.ack.emit('update', xml.serializeToString(root));
      return q.reject('failed to parse command');
    }
  }
}

module.exports = DisXmlCmd;
