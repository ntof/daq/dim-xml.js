// @ts-check
const
  { invoke } = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisXmlNode, DicXmlDataSet, XmlData } = require('../src');

describe('DisXmlDataSet', function() {
  var env/*: { dis: DisXmlNode, client?: DicXmlDataSet } */;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    invoke(env.client, 'release');
    env.dis.close();
  });

  it('can add data using index', async function() {
    var dataset = env.dis.addXmlDataSet('/pwet');
    dataset.add({ index: 2, name: 'sample1', unit: 'lux',
      type: XmlData.Type.INT64, value: 1234 });
    // added after index:2 on-purpose
    dataset.add({ index: 1, name: 'sample2', unit: 'lux',
      value: [ { index: 0, name: 'emptyNested' } ]
    });
    // Should entirely override index:1 value
    dataset.add({ index: 1, name: 'sample2', unit: 'lux',
      value: [ { index: 0, name: 'overrideNested' } ]
    });
    await dataset.synced;

    env.client = new DicXmlDataSet('/pwet', env.dis.info);
    expect(await env.client.promise()).to.deep.equal([
      { index: 1, name: 'sample2',
        value: [ {
          index: 0,
          name: 'overrideNested', value: []
        } ]
      },
      { index: 2, name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
        value: 1234 }
    ]);
  });
});
