// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),

  { DicXmlState, DisXmlNode } = require('../src'),
  { waitEvent } = require('./utils');

describe('DisXmlState', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can serve a value', async function() {
    var state = env.dis.addXmlState('/test');
    state.addState(0, "Idle");
    state.addState(1, "Initialization");
    state.setState(0);
    await state.synced;

    await DicXmlState.get('/test', env.dis.info)
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "Idle",
      value: 0,
      errors: [],
      warnings: []
    }));
  });

  it('can serve an error state', async function() {
    var state = env.dis.addXmlState('/test');
    state.addState(0, "Idle");
    state.addState(1, "Initialization");
    state.setState(0);
    state.addError(404, 'not found');
    state.addError(500, 'server failure');
    state.addWarning(808, 'woot');
    state.addWarning(808, 'woot !'); /* modify message */

    state.addState(3, 'temporary');
    state.removeState(3);
    await state.synced;

    var dic = new DicXmlState('/test', null, env.dis.info);
    await dic
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "ERROR",
      value: -2,
      errors: [
        { code: 404, message: 'not found' },
        { code: 500, message: 'server failure' }
      ],
      warnings: [
        { code: 808, message: 'woot !' }
      ]
    }))
    .then(() => {
      /* error is cleared when new state is set */
      state.setState(1);
    })
    .then(_.constant(waitEvent(dic, 'value', 2)))
    .then((value) => {
      expect(value[1]).to.have.property('value', 1);
      expect(value[1].errors).to.be.empty();
    })
    .then(() => {
      state.addError(1, 'test');
      state.addError(1, 'test with new message');
      expect(state.setState(42)).to.be.false();
    })
    .then(_.constant(waitEvent(dic, 'value', 3)))
    .then((value) => {
      expect(value[2]).to.have.property('value', -2);
      /* removing the error should restore state */
      state.removeError(1);
      expect(state.removeError(1)).to.be.false();
      expect(state.removeWarning(808)).to.be.true();
    })
    .then(_.constant(waitEvent(dic, 'value', 4)))
    .then((value) => {
      expect(value[3]).to.have.property('value', 1);
      expect(value[3].errors).to.be.empty();
    })
    .finally(() => dic.release());
  });
});
