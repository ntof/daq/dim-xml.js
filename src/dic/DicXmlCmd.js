// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('dim:client'),
  { DicCmd, DnsClient, packets, DicService, Errors } = require('@cern/dim'),
  XmlCmd = require('../XmlCmd'),
  xml = require('../xml');

/**
 * @typedef {import('@cern/dim').ServiceInfo} ServiceInfo
 * @typedef {import('@cern/dim').NodeInfo} NodeInfo
 * @typedef {import('@cern/dim').packets.DicRepInfo} DicRepInfo
 * @typedef {import('@cern/dim').DisClientRef} DisClientRef
 *
 * @typedef {import('./types').DicXmlCmd.Reply} Reply
 */

class DicXmlCmd extends DicService {
  /**
   * @param {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)?=} dns
   * @param {boolean?=} [isRpc=false]
   */
  _init(service, dns, isRpc = false) {
    /** @type {(info: ServiceInfo) => any} */
    this._dnsCallback = (info) => this.setNode(_.get(info, 'node'));
    this._parser = new xml.DOMParser();
    this._nodeDef = q.defer();
    this._prom = this._nodeDef.promise;
    this._isRpc = isRpc;
    // @ts-ignore not public
    super._init(service, dns);
  }

  /**
   * @param {(DnsClient|string)?=} dns
   */
  setDns(dns) {
    const dnsClient = DnsClient.wrap(dns);
    const name = this.name + (this._isRpc ? '/RpcIn' : '/Cmd');
    if (this.dns) {
      // @ts-ignore: always set
      this.dns.removeListener('service:' + name, this._dnsCallback);
      this.dns.unref();
    }
    this.dns = dnsClient;
    if (dnsClient) {
      dnsClient.query(name)
      .then(
        () => this.setNode(_.get(dnsClient.getServiceInfo(name), 'node')),
        (err) => debug('dns request failed:', err)
      );
      // @ts-ignore: always set
      dnsClient.on('service:' + name, this._dnsCallback);
    }
  }

  /**
   * @param {NodeInfo?=} nodeInfo
   */
  setNode(nodeInfo) {
    const ret = super.setNode(nodeInfo);
    if (ret) {
      this.once('detached', (/** @type {DisClientRef} */ node) => {
        // @ts-ignore
        node.abort(this.ackSid);
        this._nodeDef = q.defer();
        this._prom = this._prom
        .finally(_.constant(this._nodeDef.promise));
      });
      // @ts-ignore: node is set since setNode returned true
      this.ackSid = this.node.monitor(
        this.name + (this._isRpc ? '/RpcOut' : '/Ack'),
        { type: packets.DicReq.Type.MONIT_ONLY, timeout: 0 },
        this._reply.bind(this));
    }
    /*
      resolve promise, no mater what
      will unlock rpc for unknown services -> triggering an error
    */
    _.invoke(this._nodeDef, 'resolve');
    this._nodeDef = null;
    return ret;
  }

  /**
   * @param {any} args
   * @param {number?=} key
   * @param {number?=} timeout
   * @return {Q.Promise<Reply>}
   */
  invoke(args, key, timeout) {
    var def = q.defer();
    timeout = _.defaultTo(timeout, DicXmlCmd.TIMEOUT);

    /* wait for your turn */
    this._prom = this._prom
    .finally(() => {
      this._pending = def;
      key = key ? key : Math.floor(Math.random() * 2147483647);
      this._pendingKey = key;

      var root = xml.createDocument('command').documentElement;
      root.setAttribute('key', _.toString(key));
      xml.fromJs(root, args);

      return DicCmd.invoke(this.name + (this._isRpc ? '/RpcIn' : '/Cmd'),
        Buffer.from(xml.serializeToString(root) + '\0'),
        _.get(this.node, 'info', this.dns), timeout)
      .catch((/** @type {any} */ err) => {
        this._pending = null;
        def.reject(err);
      });
    });

    let timer = setTimeout(() => {
      // @ts-ignore: resetting it
      timer = null;
      this._pending = null;
      def.reject(new Errors.Timeout('request time-out'));
    }, timeout);

    return def.promise.finally(() => clearTimeout(timer));
  }

  /**
   * @param {DicRepInfo} rep
   */
  _reply(rep) {
    if (!rep || _.isEmpty(rep.data)) { return; }
    // @ts-ignore not null
    var root = this._parser.parseFromString(
      rep.data.toString()).documentElement;

    var key = _.toNumber(root.getAttribute('key'));

    if (key === this._pendingKey || key === -1) {
      /** @type {Reply} */
      var value = {
        key: key,
        status: _.toNumber(root.getAttribute('status')),
        error: _.toNumber(root.getAttribute('error')),
        xml: root
      };

      // @ts-ignore
      if (key === -1) { delete value.key; }

      if (value.status === XmlCmd.Status.OK) {
        _.invoke(this._pending, 'resolve', value);
      }
      else if (value.status === XmlCmd.Status.ERROR) {
        value.message =
          (key === -1 ? root.getAttribute('message') : xml.getText(root));
        _.invoke(this._pending, 'reject', value);
      }
      else {
        value.status = XmlCmd.Status.ERROR;
        value.error = -1;
        value.message = 'unknown error';
        _.invoke(this._pending, 'reject', value);
      }
    }
  }

  /**
   * @param {string} service
   * @param {any} args
   * @param {(DnsClient|NodeInfo|string)?=} dns
   * @param {number?=} key
   * @param {number?=} timeout
   * @return {Q.Promise<any>}
   */
  static invoke(service, args, dns, key, timeout) {
    var rpc = new DicXmlCmd(service, dns);
    return rpc.invoke(args, key, timeout)
    .finally(rpc.release.bind(rpc));
  }
}

DicXmlCmd.TIMEOUT = 1000;

module.exports = DicXmlCmd;
