// @ts-check
const
  q = require('q'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisXmlNode, DicXmlDataSet, XmlData } = require('../src');

describe('DicXmlDataSet', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can retrieve a dataset', async function() {
    var dataset = env.dis.addXmlDataSet('/pwet');
    dataset.add({ name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
      value: 1234 });
    dataset.add({ name: 'sample2', unit: 'lux', type: XmlData.Type.ENUM,
      value: 0, valueName: 'first value',
      enum: [
        { value: 0, name: 'first value' },
        { value: 1, name: 'second value' }
      ]
    });
    await dataset.synced;

    var client = new DicXmlDataSet('/pwet', env.dis.info);
    await client
    .then((value) => {
      expect(value).to.deep.equal([
        { index: 0, name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 1234 },
        { index: 1, name: 'sample2', unit: 'lux', type: XmlData.Type.ENUM,
          value: 0, valueName: 'first value',
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        }
      ]);
    })
    .then(() => {
      var def = q.defer();
      client.once('value', (value) => {
        expect(value).to.be.an('array');
        expect(value[0]).to.deep.contain({ index: 0, value: 42 });
        expect(value[1]).to.deep.contain(
          { index: 1, value: 1, valueName: 'second value' });
        def.resolve();
      });

      dataset.update({ index: 1, value: 1 });
      dataset.update({ index: 0, value: 42 }); /* only one event should be sent in deferred */
      return def.promise;
    })
    .then(() => {
      expect(dataset.get(-1)).to.be.undefined();
      expect(dataset.get(0)).to.equal(42);
    })
    .finally(() => client.release());
  });


  it('can retrieve a nested dataset', async function() {
    var dataset = env.dis.addXmlDataSet('/pwet');
    dataset.add({ index: 0, name: 'sample1', unit: 'lux',
      type: XmlData.Type.INT64, value: 1234 });
    /* $FlowIgnore: nested */
    dataset.add({
      name: "sample2",
      value: [
        {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 4321 },
        {
          name: 'nestedSample2', unit: 'lux', type: XmlData.Type.ENUM,
          value: 0, valueName: 'first value',
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        }
      ]
    });
    dataset.add({ name: 'sample3', unit: 'cm',
      type: XmlData.Type.UINT16, value: 24 });
    await dataset.synced;

    const client = new DicXmlDataSet('/pwet', env.dis.info);
    await client
    .then((value) => {
      expect(value).to.deep.equal([
        { index: 0, name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 1234 },
        { index: 1,
          name: 'sample2',
          value: [
            {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
              value: 4321, index: 0 },
            {
              name: 'nestedSample2', unit: 'lux', type: XmlData.Type.ENUM,
              value: 0, valueName: 'first value', index: 1,
              enum: [
                { value: 0, name: 'first value' },
                { value: 1, name: 'second value' }
              ]
            }
          ] },
        { index: 2, name: 'sample3', unit: 'cm',
          type: XmlData.Type.UINT16, value: 24 }
      ]);
    })
    .then(() => {
      var def = q.defer();
      client.once('value', (value) => {
        expect(value).to.be.an('array');
        expect(value[0]).to.deep.contain({ index: 0, value: 12 });
        expect(value[1]).to.deep.contain({ name: 'sample2',
          value: [
            {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
              value: 5432, index: 0 },
            {
              name: 'nestedSample2', unit: 'lux', type: XmlData.Type.ENUM,
              value: 1, valueName: 'second value', index: 1,
              enum: [
                { value: 0, name: 'first value' },
                { value: 1, name: 'second value' }
              ]
            }
          ] });
        expect(value[2]).to.deep.contain(
          { index: 2, value: 8 });
        def.resolve();
      });

      expect(dataset.update(4, 8)).to.be.false();
      expect(dataset.update(2, 8)).to.be.true();
      expect(dataset.update({ index: 1, value: [
        {
          index: 0, value: 5432, type: XmlData.Type.INT64
        },
        {
          index: 1, value: 1
        }
      ] })).to.be.true();
      expect(dataset.update({ index: 0, value: 12 })).to.be.true(); /* only one event should be sent in deferred */
      return def.promise;
    })
    .then(() => {
      expect(dataset.get(-1)).to.be.undefined();
      expect(dataset.getNested(5)).to.be.undefined();
      expect(dataset.getNested(0, 2)).to.be.undefined();
      expect(dataset.get(0)).to.equal(12);
      expect(dataset.getNested(1)).to.deep.equal([
        { name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 5432, index: 0 },
        { name: 'nestedSample2', unit: 'lux', type: XmlData.Type.ENUM,
          value: 1, valueName: 'second value', index: 1,
          enum: [
            { value: 0, name: 'first value' },
            { value: 1, name: 'second value' }
          ]
        }
      ]);

      expect(dataset.remove(6)).to.be.false();
      expect(dataset.remove(3, 5)).to.be.false();
      expect(dataset.remove(0, 1)).to.be.false();
      expect(dataset.remove(0)).to.be.true();
      expect(dataset.remove(1, 1)).to.be.true();
      expect(dataset.get(0)).to.be.undefined();
      expect(dataset.getNested(1)).to.deep.equal([
        {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 5432, index: 0 }
      ]);
    })
    .finally(() => client.release());
  });

  it('can retrieve a single value', async function() {
    var dataset = env.dis.addXmlDataSet('/pwet');
    dataset.add({ name: 'sample1', unit: 'lux', type: XmlData.Type.INT8,
      value: 123 });
    dataset.add({ name: 'sample2', unit: 'MHz', type: XmlData.Type.FLOAT,
      value: 1.5 });
    dataset.add({ name: 'sample3', unit: '', type: XmlData.Type.BOOL,
      value: false });
    await dataset.synced;

    await DicXmlDataSet.get('/pwet', env.dis.info)
    .then((value) => {
      expect(value).to.deep.equal([
        { index: 0, name: 'sample1', unit: 'lux', type: XmlData.Type.INT8,
          value: 123 },
        { index: 1, name: 'sample2', unit: 'MHz', type: XmlData.Type.FLOAT,
          value: 1.5 },
        { index: 2, name: 'sample3', type: XmlData.Type.BOOL, value: false }
      ]);
    });

  });

  it('can override nested dataset', async function() {
    var dataset = env.dis.addXmlDataSet('/pwet');
    dataset.add({ index: 0, name: 'sample1', unit: 'lux',
      type: XmlData.Type.INT64, value: 1234 });
    /* $FlowIgnore: nested */
    dataset.add({
      name: "sample2",
      value: [
        {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 1 },
        {  name: 'nestedSample2',
          value: [
            {  name: 'nestedSample3', unit: 'lux', type: XmlData.Type.INT64,
              value: 2 }
          ] }
      ]
    });
    await dataset.synced;

    const client = new DicXmlDataSet('/pwet', env.dis.info);
    await client
    .then((value) => {
      expect(value).to.deep.equal([
        { index: 0, name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 1234 },
        { index: 1,
          name: 'sample2',
          value: [
            { index: 0, name: 'nestedSample1', unit: 'lux',
              type: XmlData.Type.INT64, value: 1 },
            { index: 1, name: 'nestedSample2',
              value: [
                {  index: 0, name: 'nestedSample3', unit: 'lux',
                  type: XmlData.Type.INT64, value: 2 }
              ] }
          ] }
      ]);
    })
    .then(() => {
      var def = q.defer();
      client.once('value', (value) => {
        expect(value).to.be.an('array');
        expect(value[0]).to.deep.contain({ index: 0, value: "String" });
        expect(value[1]).to.deep.contain({ name: 'sample2',
          value: [
            {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
              value: 5432, index: 0 },
            { index: 1, name: 'nestedSample2', type: XmlData.Type.STRING,
              value: "OverrideNow" }
          ] });
        def.resolve();
      });

      expect(dataset.update([
        { index: 0, value: "String",
          type: XmlData.Type.STRING },
        { index: 1, value: [
          {
            index: 0, value: 5432, type: XmlData.Type.INT64
          },
          {
            index: 1, value: "OverrideNow", type: XmlData.Type.STRING
          }
        ] }
      ], true)).to.be.true();
      return def.promise;
    })
    .then(() => {
      expect(dataset.get(0)).to.equal("String");
      expect(dataset.getNested(1)).to.deep.equal([
        {  name: 'nestedSample1', unit: 'lux', type: XmlData.Type.INT64,
          value: 5432, index: 0 },
        {
          name: 'nestedSample2', type: XmlData.Type.STRING,
          value: "OverrideNow", index: 1
        }
      ]);
    })
    .finally(() => client.release());
  });
});
