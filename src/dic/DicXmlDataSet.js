// @ts-check
const
  _ = require('lodash'),
  { DicValue } = require('@cern/dim'),

  xml = require('../xml'),
  XmlData = require('../XmlData'),
  DicXmlValue = require('./DicXmlValue');

/**
 * @typedef {import('@cern/dim').ServiceInfo} ServiceInfo
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@cern/dim').NodeInfo} NodeInfo
 *
 * @typedef {import('../types').XmlData} XmlData
 * @typedef {import('../types').XmlDataSet} XmlDataSet
 * @typedef {import('../types').XmlEnumDesc} XmlEnumDesc
 */

/**
 * @param {Element} e
 * @return {XmlEnumDesc}
 */
function parseEnum(e) {
  return _.map(xml.getChildNodes(e, 'value'), function(elt) {
    return {
      value: _.toNumber(elt.getAttribute('value')),
      name: elt.getAttribute('name') || ''
    };
  });
}

/**
 * @param {Element} data
 * @return {XmlData}
 */
function parseData(data) {
  if (_.isEmpty(data.getAttribute('type')) &&
    _.isEmpty(data.getAttribute('value'))) {
    /** @type {XmlData} */
    const ret = {
      value: _.map(xml.getChildNodes(data, 'data'), parseData),
      index: _.toNumber(data.getAttribute('index'))
    };
    if (data.hasAttribute('name')) {
      // @ts-ignore checked above
      ret.name = data.getAttribute('name');
    }
    return ret;
  }
  else {
    /** @type {XmlData} */
    const ret = {
      type: _.toNumber(data.getAttribute('type')),
      value: data.getAttribute('value'),
      index: _.toNumber(data.getAttribute('index'))
    };
    if (data.hasAttribute('name')) {
      // @ts-ignore checked above
      ret.name = data.getAttribute('name');
    }
    if (data.hasAttribute('unit')) {
      // @ts-ignore checked above
      ret.unit = data.getAttribute('unit');
    }
    /* @ts-ignore set above */
    ret.value = XmlData.parseValue(ret.value, ret.type);
    if (ret.type === XmlData.Type.ENUM) {
      ret.valueName = data.getAttribute('valueName') || '';
      ret.enum = parseEnum(xml.getChildNodes(data, 'enum')[0]);
    }
    return ret;
  }
}

/**
 * @extends DicValue<DicXmlDataSet>
 */
class DicXmlDataSet extends DicValue {
  /**
   * @param {string|ServiceInfo} service
   * @param {{ timeout?: number, stamped?: boolean }?=} options
   * @param {(DnsClient|NodeInfo|string)?=} dns
   */
  constructor(service, options, dns) {
    super(service, options, dns);
    this._parser = new xml.DOMParser();
  }

  /**
   * @param {any} rep
   */
  _setValue(rep) {
    if (rep && rep.data) {
      rep.value = DicXmlDataSet.parse(rep.data.toString(), this._parser);
    }
    // @ts-ignore internal api
    super._setValue(rep);
  }

  /**
   * @param {string} xmlsource
   * @param {DOMParser?=} parser
   * @return {XmlDataSet|null}
   */
  static parse(xmlsource, parser) {
    var root = DicXmlValue.parse(xmlsource, parser);
    if (!root) {
      return null;
    }
    return _.map(xml.getChildNodes(root, 'data'), parseData);
  }

  /**
   * @template T=XmlDataSet
   * @param {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)?} dns
   * @param {number?=} timeout
   * @return {Promise<T>}
   */
  static async get(service, dns, timeout) {
    const rep = await DicValue.get(service, dns, timeout);
    var ret = DicXmlDataSet.parse(rep || '');
    if (_.isNil(ret)) {
      throw new Error('failed to parse document');
    }
    // @ts-ignore
    return ret;
  }
}

module.exports = DicXmlDataSet;
