// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisNode, ServiceInfo } = require('@cern/dim'),

  { waitEvent } = require('./utils'),
  { DicXmlValue, xml } = require('../src');

/*::
  import type { $Element } from 'xmldom'
*/

describe('DicXmlState', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can retrieve a value', function() {
    env.dis.addService('/test', 'C', `
      <state value="0" strValue="Idle">
        <value value="-2" strValue="ERROR">test</value>
      </state>`);

    return DicXmlValue.get(new ServiceInfo('/test', 'C'), env.dis.info)
    .then((value) => {
      expect(xml.toJs(value)).to.deep.contain({
        "$": { strValue: "Idle", value: "0" },
        value: {
          "$textContent": "test",
          "$": { strValue: "ERROR", value: "-2" }
        }
      });
    });
  });

  it('can track a value', function() {
    /* $FlowIgnore */
    var service /*: DisService */ = env.dis.addService('/test', 'C',
      '<toto value="42" />');

    var client = new DicXmlValue(new ServiceInfo('/test', 'C'), env.dis.info);
    return client.promise()
    .then((value /*: $Element */) => {
      expect(value).to.have.property('tagName', 'toto');

      return q()
      .then(() => service.setValue('invalid xml'))
      .then(_.constant(waitEvent(client, 'value', 1)))
      .then(() => expect(client.value).to.be.null())
      .then(() => service.setValue('<toto value="42" />'))
      .then(_.constant(waitEvent(client, 'value', 2)))
      .then(() => expect(client.value).to.be.not.null())
      .then(() => service.setValue(null))
      .then(_.constant(waitEvent(client, 'value', 3)))
      .then(() => expect(client.value).to.be.null());
    })
    .finally(() => client.release());
  });
});
