// @ts-check

const
  // dic
  DicXmlCmd = require('./dic/DicXmlCmd'),
  DicXmlDataSet = require('./dic/DicXmlDataSet'),
  DicXmlParams = require('./dic/DicXmlParams'),
  DicXmlRpc = require('./dic/DicXmlRpc'),
  DicXmlState = require('./dic/DicXmlState'),
  DicXmlValue = require('./dic/DicXmlValue'),
  // dis
  DisXmlCmd = require('./dis/DisXmlCmd'),
  DisXmlDataSet = require('./dis/DisXmlDataSet'),
  DisXmlNode = require('./dis/DisXmlNode'),
  DisXmlParams = require('./dis/DisXmlParams'),
  DisXmlRpc = require('./dis/DisXmlRpc'),
  DisXmlState = require('./dis/DisXmlState'),
  // utils
  XmlCmd = require('./XmlCmd'),
  XmlData = require('./XmlData'),
  xml = require('./xml');

module.exports = {
  DicXmlCmd, DicXmlDataSet, DicXmlParams, DicXmlRpc, DicXmlState, DicXmlValue,
  DisXmlCmd, DisXmlDataSet, DisXmlNode, DisXmlParams, DisXmlRpc, DisXmlState,
  XmlCmd, XmlData, xml };
