
export as namespace dicXml;

import XmlCmd from '../XmlCmd'

export namespace DicXmlCmd {
  export interface Reply {
    key: number,
    status: XmlCmd.Status,
    error: number,
    xml: Element,
    message?: ?string
  }
}
