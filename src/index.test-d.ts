
import { expectType } from 'tsd';

import { DnsClient, ServiceInfo, NodeInfo, DisService } from '@cern/dim';
import {
  DicXmlCmd, DicXmlDataSet, DicXmlParams, DicXmlRpc, DicXmlState, DicXmlValue,
  XmlDataSet, XmlState, XmlCmd, XmlData,
  DisXmlNode, DisXmlCmd, DisXmlRpc, DisXmlState, DisXmlDataSet, DisXmlParams
 } from '.';

/* DIC */
(async function() {
  var dnsClient: DnsClient = new DnsClient('dnsserver.cern.ch', 2505);
  await DicXmlCmd.invoke('/test', { test: 42 }, 'dnsserver.cern.ch:2505', 1000);

  var cmd: DicXmlCmd = new DicXmlCmd('/test', dnsClient);
  await cmd.invoke({ test: 42 });

  var dataset: DicXmlDataSet = new DicXmlDataSet('/test', null, dnsClient);
  expectType<XmlDataSet|void>(await dataset.promise());
  expectType<XmlDataSet|null>(DicXmlDataSet.parse('<xml>'));

  var params: DicXmlParams = new DicXmlParams('/test', null, dnsClient);
  expectType<XmlDataSet|void>(await params.promise());
  expectType<DicXmlCmd.Reply>(
    await params.setParams([ { value: 12, index: 3 } ]));

  var rpc = new DicXmlRpc(new ServiceInfo('/test', 'C;C|RPC', 1,
    NodeInfo.local('localhost', 1234)));
  expectType<DicXmlCmd.Reply>(
    await rpc.invoke({ test: 42 }));

  var state = new DicXmlState('/state', { stamped: true }, dnsClient);
  expectType<XmlState|void>(await state.promise());
  expectType<XmlState|void>(await DicXmlState.get('/test', 'proxy-1'));

  var value: DicXmlValue = new DicXmlValue('/test', null, dnsClient);
  expectType<Element|void>(await value.promise());
  expectType<Element|void>(await DicXmlValue.get('/test', dnsClient));

  dnsClient.unref();
}());

/* DIS */
(async function() {
  var dnsClient: DnsClient = new DnsClient('dnsserver.cern.ch', 2505);
  var node = new DisXmlNode();
  await node.listen();
  await node.register(dnsClient);

  expectType<DisXmlRpc|null>(
    node.addXmlRpc('/test', (req: DisXmlCmd.ReqInfo, ret: Element) => undefined));
  node.removeXmlRpc('/test');
  node.addXmlRpc('/test',
    (req: DisXmlCmd.ReqInfo) => XmlCmd.createError(-1, 'error'))

  expectType<DisXmlDataSet>(node.addXmlDataSet('/test'));
  node.removeXmlDataSet('/test');

  expectType<DisXmlParams>(node.addXmlParams('/test'));
  node.removeXmlParams('/test');
  var dataset = new DisXmlParams();
  dataset.add({ name: 'test', value: 42, type: XmlData.Type.INT64 });
  dataset.update(0, 44, true);
  dataset.update({ index: 0, value: 44 });

  var state: DisXmlState = node.addXmlState('/test')
  state.addState(1, 'test');
  state.addError(12, 'test');
  state.removeError(12);
  state.setState(1);

  expectType<DisService<string>|null>(node.addService('test', 'C', 'test'));

  node.close();
}());