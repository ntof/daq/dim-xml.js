# GetXmlValue.js

Utility to get prettified XML Values from a Dim Service.

It provides one-shot get and listening mode.

```bash
Usage: GetXmlValue [options]

Options:
  -s, --service <service-path>  provide service path for value (Ex. TEST/Aqn)
  -l, --listen                  enable listening mode
  -p, --proxy <proxy-url>       provide proxy url (default: "tcp://localhost:2505")
  -t, --timeout [seconds]       provide call timeout
  -h, --help                    output usage information
```

Examples:

``` bash
# run
node GetXmlValue.js --service MERGER/Current/Aqn --listen
```

# SendXmlCmd.js

Utility to send XML Commands to a Dim Command Service.

It provides the possibility to listen for an answer on the relative `/Ack` service

```bash
Usage: SendXmlCmd [options]

Options:
  -s, --service <service-path>  provide service path for command (Ex. TEST/Cmd)
  -c, --cmd <xml-command>       provide xml command to send (<command key="123">...</command>)
  -l, --listen                  enable listening mode for Ack
  -p, --proxy <proxy-url>       provide proxy url (default: "tcp://localhost:2505")
  -t, --timeout [seconds]       provide call timeout
  -h, --help                    output usage information
```

Examples:

``` bash
# run
node SendXmlCmd -s MERGER/Current/Cmd --cmd \
"<command key='1234'> \
  <parameters>  \
    <data name='runNumber' index='1' type='14' value='900003'/> \
    <data name='experiment' index='2' type='3' value='test03'/> \
    <data name='approved' index='3' type='9' value='1'/> \
  </parameters>  \
</command>"
```
