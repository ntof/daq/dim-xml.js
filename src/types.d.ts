
export as namespace dimXml

export interface XmlJsAttrList { [attrName:string]: any }

export interface XmlJsObject {
  '$'?: XmlJsAttrList
  '$textContent'?: string
  [tagName:string]: Array<XmlJsObject> | XmlJsObject | any | string
}

export interface XmlData {
  name?: string,
  index: number,
  unit?: string,
  type?: XmlDataType,
  value: any,
  valueName?: string,

  enum?: XmlEnumDesc
}

export type XmlDataType = number
export type XmlDataSet = Array<XmlData>
export type XmlEnumDesc = Array<{ name: string, value: number }>

export interface XmlParamPart {
  value: string|number|boolean|Array<XmlParamPart>,
  type?: XmlDataType,
  index: number
}

export interface XmlState extends XmlState.State {
  errors: Array<XmlState.Error>,
  warnings: Array<XmlState.Error>,
  description: Array<XmlState.State>
}

export namespace XmlState {
  export interface Error {
    code: number,
    message: string
  }
  
  export interface State {
    value: number,
    strValue: string
  }
}