// @ts-check

/**
 * @typedef {import('./dis/types').DisXmlCmd.Reply} DisXmlCmdReply
 */

const XmlCmd = {
  ERROR_NONE: 0,
  /** @enum {number} */
  Status: {
    OK: 2,
    ERROR: 3
  },

  /**
   * @param {number} error
   * @param {string} message
   * @return {DisXmlCmdReply}
   */
  createError: function(error, message) {
    return { status: XmlCmd.Status.ERROR, error: error, message: message };
  }
};

module.exports = XmlCmd;
