// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  debug = require('debug')('dim:server:xml'),
  xml = require('../xml'),
  XmlCmd = require('../XmlCmd'),
  { DisRpc } = require('@cern/dim');

/**
 * @typedef {import('./types').DisXmlCmd.Reply} Reply
 * @typedef {import('./types').DisXmlCmd.ReqInfo} ReqInfo
 * @typedef {import('@cern/dim').DicReqInfo} DicReqInfo
 */

class DisXmlRpc {
  /**
   * @param {(req: ReqInfo, ret: Element) => Reply|void} callback
   */
  constructor(callback) {
    this.value = callback;
    this.rpc = new DisRpc('C,C', this.handle.bind(this));
    this._parser = new xml.DOMParser();
  }

  /**
   * @param  {DicReqInfo} req
   * @return {Q.Promise<void>}
   */
  handle(req) {
    var def = q.defer();
    if (req && req.value) {
      /** @type {ReqInfo} */ (req).xml =
        this._parser.parseFromString(req.value).documentElement;
      var root = xml.createDocument('acknowledge').documentElement;
      root.setAttribute('key',
      /** @type {ReqInfo} */ (req).xml.getAttribute('key') || '');

      q()
      .then(() => _.invoke(this, 'value', req, root))
      .then(
        (ret) => {
          root.setAttribute('status', _.toString(XmlCmd.Status.OK));
          root.setAttribute('error', _.toString(XmlCmd.ERROR_NONE));
          if (ret) {
            root.appendChild(root.ownerDocument.createTextNode(
              _.toString(ret)));
          }
        },
        (ret) => {
          debug('cmd failed:', ret);
          root.setAttribute('status',
            _.get(ret, 'status', XmlCmd.Status.ERROR));
          root.setAttribute('error', _.get(ret, 'error', -1));
          root.appendChild(root.ownerDocument.createTextNode(
            _.get(ret, 'message', _.toString(ret))));
        }
      )
      .finally(() => def.resolve(xml.serializeToString(root)));
    }
    else {
      debug('failed to parse command:', req.value);
      const root = xml.createDocument('acknowledge').documentElement;
      root.setAttribute('key', "-1"); // Error mode when we can't parse key
      xml.fromJs(root,
        { "$": XmlCmd.createError(-1, 'failed to parse command') });
      def.resolve(xml.serializeToString(root));
    }

    return def.promise;
  }
}

module.exports = DisXmlRpc;
