// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisXmlNode, DicXmlCmd } = require('../src'),
  { DnsServer } = require('@cern/dim');

describe('DicXmlCmd', function() {
  var env;

  beforeEach(async function() {
    env = {
      dis: new DisXmlNode(),
      dns: new DnsServer(null, 0)
    };
    await env.dis.listen();
    await env.dns.listen();
  });

  afterEach(function() {
    env.dis.close();
    env.dns.close();
  });

  it('can make a call when service appears', async function() {
    env.dis.addXmlCmd('/pwet', _.noop);

    var cmd = new DicXmlCmd('/pwet', env.dns.url());
    var def = q.defer();
    cmd.once('attached', () => def.resolve());

    try {
      /* registered after DicXmlCmd creation */
      await env.dis.register(env.dns.url());

      await def.promise;

      const ret = await cmd.invoke({ pwet: 42 }, 1234);
      expect(ret).to.deep.contain({ error: 0, key: 1234, status: 2 });

      expect(cmd.name).to.equal('/pwet');
    }
    finally {
      cmd.release();
    }
  });
});
