// @ts-check
const
  _ = require('lodash'),
  { DicValue } = require('@cern/dim'),

  xml = require('../xml');

/**
 * @typedef {import('@cern/dim').ServiceInfo} ServiceInfo
 * @typedef {import('@cern/dim').DnsClient} DnsClient
 * @typedef {import('@cern/dim').NodeInfo} NodeInfo
 */

/**
 * @extends DicValue<Element>
 */
class DicXmlValue extends DicValue {
  /**
   * @param {string|ServiceInfo} service
   * @param {{ timeout?: number, stamped?: boolean }?=} options
   * @param {(DnsClient|NodeInfo|string)?=} dns
   */
  constructor(service, options, dns) {
    super(service, options, dns);
    this._parser = new xml.DOMParser();
  }

  /**
   * @param {any} rep
   */
  _setValue(rep) {
    if (rep && rep.data) {
      rep.value = DicXmlValue.parse(rep.data.toString(), this._parser);
    }
    // @ts-ignore internal API
    super._setValue(rep);
  }

  /**
   * @param {string|Buffer} xmlsource
   * @param {DOMParser?=} parser
   * @return {?Element}
   */
  static parse(xmlsource, parser) {
    if (!parser) {
      parser = new xml.DOMParser();
    }
    if (xmlsource instanceof Buffer) {
      xmlsource = xmlsource.toString();
    }
    if (_.isEmpty(xmlsource)) {
      return null;
    }
    // @ts-ignore
    var doc = parser.parseFromString(xmlsource);
    return doc ? doc.documentElement : null;
  }

  /**
   * @template T=Element
   * @param {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)?} dns
   * @param {number?=} timeout
   * @return {Promise<T>}
   */
  static async get(service, dns, timeout) {
    const rep = await DicValue.get(service, dns, timeout);
    var ret = DicXmlValue.parse(rep || '');
    if (_.isNil(ret)) {
      throw new Error('failed to parse document');
    }
    // @ts-ignore
    return ret;
  }
}

module.exports = DicXmlValue;
