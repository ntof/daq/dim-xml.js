// @ts-check
const
  debug = require('debug')('dim:xml'),
  { DisNode } = require('@cern/dim'),

  DisXmlCmd = require('./DisXmlCmd'),
  DisXmlRpc = require('./DisXmlRpc'),
  DisXmlDataSet = require('./DisXmlDataSet'),
  DisXmlParams = require('./DisXmlParams'),
  DisXmlState = require('./DisXmlState');

/**
 * @typedef {import('./types').DisXmlCmd.Reply} Reply
 * @typedef {import('./types').DisXmlCmd.ReqInfo} ReqInfo
 */

/**
 * @param  {DisXmlCmd|((req: ReqInfo, ret: Element) => Reply|void)} cb
 * @return {?DisXmlCmd}
 */
function createDisXmlCmd(cb) {
  let service = null;
  if (cb instanceof DisXmlCmd) {
    service = cb;
  }
  else if (cb) {
    service = new DisXmlCmd(cb);
  }
  return service;
}

/**
 * @param  {DisXmlRpc|((req: ReqInfo, ret: Element) => Reply|void)} cb
 * @return {?DisXmlRpc}
 */
function createDisXmlRpc(cb) {
  let service = null;
  if (cb instanceof DisXmlRpc) {
    service = cb;
  }
  else if (cb) {
    service = new DisXmlRpc(cb);
  }
  return service;
}

class DisXmlNode extends DisNode {
  /**
   * @param  {string} name
   * @param  {DisXmlRpc|((req: ReqInfo, ret: Element) => Reply)} cb
   * @return {?DisXmlRpc}
   */
  addXmlRpc(name, cb) {
    if (this.isService(name + '/RpcIn') || this.isService(name + '/RpcOut')) {
      debug('Rpc service already registered:', name);
      return null;
    }

    const service = createDisXmlRpc(cb);
    if (!service) {
      return null;
    }

    /* @ts-ignore: It won't be undefined */
    this.addCmd(name + '/RpcIn', service.rpc.rpcIn);
    /* @ts-ignore: It won't be undefined */
    this.addService(name + '/RpcOut', service.rpc.rpcOut);
    return service;
  }

  /**
   * @param  {string} name
   */
  removeXmlRpc(name) {
    this.removeService(name + '/RpcIn');
    this.removeService(name + '/RpcOut');
  }

  /**
   * @param  {string} name
   * @param  {DisXmlCmd|((req: ReqInfo, ret: Element) => Reply)} cb
   * @return {?DisXmlCmd}
   */
  addXmlCmd(name, cb) {
    if (this.isService(name + '/Cmd') || this.isService(name + '/Ack')) {
      debug('service already registered:', name);
      return null;
    }

    const service = createDisXmlCmd(cb);
    if (!service) {
      return null;
    }

    this.addCmd(name + '/Cmd', service.cmd);
    this.addService(name + '/Ack', service.ack);
    return service;
  }

  /**
   * @param  {string} name
   */
  removeXmlCmd(name) {
    this.removeService(name + '/Cmd');
    this.removeService(name + '/Ack');
  }

  /**
   * @param  {string} name
   * @param  {DisXmlDataSet?=} dataset
   * @return {DisXmlDataSet?}
   */
  addXmlDataSet(name, dataset) {
    if (this.isService(name)) {
      debug('service already registered:', name);
      return null;
    }

    var service = dataset || new DisXmlDataSet();
    return this.addService(name, service) ? service : null;
  }

  /**
   * @param  {string} name
   */
  removeXmlDataSet(name) {
    this.removeService(name);
  }

  /**
   * @param  {string} name
   * @param  {DisXmlState?=} state
   * @return {DisXmlState?}
   */
  addXmlState(name, state) {
    if (this.isService(name)) {
      debug('service already registered:', name);
      return null;
    }

    var service = state || new DisXmlState();
    return this.addService(name, service) ? service : null;
  }

  /**
   * @param  {string} name
   */
  removeXmlState(name) {
    this.removeService(name);
  }

  /**
   * @param  {string} name
   * @param  {DisXmlParams?=} params
   * @return {DisXmlParams?}
   */
  addXmlParams(name, params) {
    if (this.isService(name + '/Aqn')) {
      debug('service already registered:', name);
      return null;
    }

    var service = params || new DisXmlParams();
    return this.addService(name + '/Aqn', service) &&
      this.addXmlCmd(name, service.cmd) ? service : null;
  }

  /**
   * @param  {string} name
   */
  removeXmlParams(name) {
    this.removeService(name + '/Aqn');
    this.removeXmlCmd(name);
  }
}

module.exports = DisXmlNode;
