// @ts-check

const _ = require('lodash');

/**
 * @typedef {import('./types').XmlDataType} XmlDataType
 */

/** @enum {number} */
const DataType = {
  INVALID: -1,
  NESTED: 0,
  INT32: 1,
  DOUBLE: 2,
  STRING: 3,
  INT64: 4,
  ENUM: 5,
  INT8: 6,
  INT16: 7,
  FLOAT: 8,
  BOOL: 9,
  UINT32: 11,
  UINT64: 14,
  UINT8: 16,
  UINT16: 17
};

const XmlData = {
  /**
   * @param  {any} value
   * @param {XmlDataType} type
   * @return {boolean|number|string}
   */
  /* eslint-disable-next-line complexity */
  parseValue: function(value, type) {
    switch (type) {
    case XmlData.Type.INT64:
    case XmlData.Type.INT32:
    case XmlData.Type.INT16:
    case XmlData.Type.INT8:
    case XmlData.Type.DOUBLE:
    case XmlData.Type.FLOAT:
    case XmlData.Type.ENUM:
    case XmlData.Type.UINT32:
    case XmlData.Type.UINT64:
    case XmlData.Type.UINT8:
    case XmlData.Type.UINT16:
      return _.toNumber(value);
    case XmlData.Type.BOOL:
      return _.toNumber(value) > 0;
    default:
      return value;
    }
  },
  Type: DataType
};

module.exports = XmlData;
