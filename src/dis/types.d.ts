export as namespace disXml;

import XmlCmd from '../XmlCmd'
import { DicReqInfo } from '@cern/dim'
import { XmlData, XmlDataType } from '../types'

export namespace DisXmlCmd {
  export interface Reply {
    key?: number,
    status: XmlCmd.Status,
    error: number,
    message?: ?string
  }

  export interface ReqInfo extends DicReqInfo {
    xml: Element
  }
}

export interface XmlDataInPart extends XmlData {
  index?: number
}

export namespace DisXmlDataSet {
  export interface UpdatePart {
    index: number,
    value: number|string|Array<UpdatePart>|boolean,
    type?: XmlDataType
  }
}