// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisNode, ServiceInfo } = require('@cern/dim'),

  { DicXmlState } = require('../src');

describe('DicXmlState', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can retrieve a value', function() {
    env.dis.addService('/test', 'C', `
      <state value="0" strValue="Idle">
        <value value="-2" strValue="ERROR"/>
        <value value="-1" strValue="NOT READY"/>
        <value value="0" strValue="Idle"/>
        <value value="1" strValue="Initialization"/>
        <error active="false" code="0"/>
        <errors/>
        <warnings/>
      </state>`);

    return DicXmlState.get(new ServiceInfo('/test', 'C'), env.dis.info)
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "Idle",
      value: 0,
      errors: [],
      warnings: []
    }));
  });

  it('can retrieve an error state', function() {
    env.dis.addService('/test', 'C', `
      <state value="-2" strValue="ERROR">
        <value value="-2" strValue="ERROR"/>
        <value value="-1" strValue="NOT READY"/>
        <value value="0" strValue="Idle"/>
        <value value="1" strValue="Initialization"/>
        <error active="false" code="0"/>
        <errors>
          <error code="404">not found</error>
          <error code="500">server failure</error>
        </errors>
        <warnings>
          <warning code="808">woot !</warning>
        </warnings>
      </state>`);
    var state = new DicXmlState(new ServiceInfo('/test', 'C'), null,
      env.dis.info);
    return state
    .then((value) => expect(value).to.deep.equal({
      description: [
        { strValue: "ERROR", value: -2 },
        { strValue: "NOT READY", value: -1 },
        { strValue: "Idle", value: 0 },
        { strValue: "Initialization", value: 1 }
      ],
      strValue: "ERROR",
      value: -2,
      errors: [
        { code: 404, message: 'not found' },
        { code: 500, message: 'server failure' }
      ],
      warnings: [
        { code: 808, message: 'woot !' }
      ]
    }))
    .finally(() => state.release());
  });
});
