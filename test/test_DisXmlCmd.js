// @ts-check
const
  _ = require('lodash'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisService, DisCmd } = require('@cern/dim'),

  { DisXmlNode } = require('../src');

describe('DisXmlCmd', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can register a cmd service', function() {
    var cmd = env.dis.addXmlCmd('/pwet', _.noop);

    if (!cmd) { throw 'should have a cmd'; }

    expect(cmd.ack).to.be.instanceof(DisService);
    expect(cmd.cmd).to.be.instanceof(DisCmd);
    expect(env.dis.isService('/pwet/Cmd')).to.be.true();
    expect(env.dis.isService('/pwet/Ack')).to.be.true();
  });
});
