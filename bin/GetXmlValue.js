#!/usr/bin/env node

const { DicValue } = require('@cern/dim');
const cmd = require('commander');
const { printXml } = require('./utils');

let opts = cmd
.option('-s, --service <service-path>', 'provide service path for value (Ex. TEST/Aqn)')
.option('-l, --listen', 'enable listening mode')
.option('-p, --proxy <proxy-url>', 'provide proxy url', 'tcp://localhost:2505')
.option('-t, --timeout [seconds]', 'provide call timeout')
.parse(process.argv);

if (!opts.service) {
  console.info("mandatory argument --service <service-path> is missing.");
  return -1;
}

let options = { timeout: undefined };

if (opts.timeout) {
  options.timeout = opts.timeout;
}

const printToConsole = (value) => {
  console.info(`\n \t ${opts.service} from ${opts.proxy} \n`);
  if (!value) { console.info('Got nothing!');}
  else { printXml(value); }
};

if (opts.listen) {
  const val = new DicValue(opts.service, options, opts.proxy);
  val.on('value', (value) => {
    console.clear();
    printToConsole(value);
  });
}
else {
  DicValue.get(opts.service, opts.proxy, options.timeout)
  .then((value) => printToConsole(value));
}

