#!/usr/bin/env node

const { DicValue, DicCmd } = require('@cern/dim');
const cmd = require('commander');
const { printXml } = require('./utils');

let opts = cmd
.option('-s, --service <service-path>', 'provide service path for command (Ex. TEST/Cmd)')
.option('-c, --cmd <xml-command>', 'provide xml command to send (<command key="123">...</command>)')
.option('-l, --listen', 'enable listening mode for Ack')
.option('-p, --proxy <proxy-url>', 'provide proxy url', 'tcp://localhost:2505')
.option('-t, --timeout [seconds]', 'provide call timeout')
.parse(process.argv);

if (!opts.service) {
  console.info("mandatory argument --service <service-path> is missing.");
  return -1;
}

if (!opts.cmd) {
  console.info("mandatory argument --cmd <xml-command> is missing.");
  return -1;
}

let options = { timeout: undefined };

if (opts.timeout) {
  options.timeout = opts.timeout;
}

const printToConsole = (service, value) => {
  console.info(`\n \t ${service} from ${opts.proxy} \n`);
  if (!value) { console.info('Got nothing!');}
  else { printXml(value); }
};

if (opts.listen) {
  let ackService = opts.service.replace(/Cmd/, 'Ack');
  console.info('ackService', ackService);
  const val = new DicValue(ackService, options, opts.proxy);
  val.on('value', (value) => {
    printToConsole(ackService, value);
    val.removeAllListeners('value');
    return;
  });
}

console.clear();
console.info(`Sending Command to ${opts.service} with xml:\n`);
printXml(opts.cmd);


DicCmd.invoke(opts.service, opts.cmd, opts.proxy, options.timeout)
.then((ack) => console.log('Command sent successfully. Return value:', ack));
