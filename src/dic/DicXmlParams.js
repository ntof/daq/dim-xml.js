// @ts-check
const
  _ = require('lodash'),

  XmlData = require('../XmlData'),
  DicXmlCmd = require('./DicXmlCmd'),
  DicXmlDataSet = require('./DicXmlDataSet'),

  { DnsClient, NodeInfo } = require('@cern/dim');

/**
 * @typedef {import('../types').XmlParamPart} XmlParamPart
 * @typedef {import('./types').DicXmlCmd.Reply} Reply
 */

/**
 * @param  {XmlParamPart} param
 * @return {any}
 */
function paramPartToXml(param) {
  param = _.clone(param);
  if (_.get(param, 'type') === XmlData.Type.BOOL) {
    param.value = param.value ? 1 : 0;
  }
  if (_.isArray(param.value)) {
    const keepValue = _.clone(param.value);
    /* @ts-ignore */
    delete param.value;
    /** @type {Array<any>} */
    const data = [];
    _.forEach(keepValue, (value) => {
      const par = paramPartToXml(value);
      data.push(par);
    });
    return { '$': param, data: data };
  }
  return { '$': param };
}

class DicXmlParams extends DicXmlDataSet {
  /**
   * @param {string} service
   * @param {{ timeout?: number, stamped?: boolean }?=} options
   * @param {(DnsClient|NodeInfo|string)?=} dns
   */
  constructor(service, options, dns) {
    if (_.isString(options) ||
        (options instanceof DnsClient) ||
        (options instanceof NodeInfo)) {
      /* $FlowIgnore */
      dns = options;
      options = null;
    }
    var name = _.get(service, 'name', service);
    super(name + '/Aqn', options, dns);
    this.cmd = new DicXmlCmd(name, dns);
  }

  release() {
    this.cmd.release();
    super.release();
  }

  /**
   * @param {Array<XmlParamPart>|XmlParamPart} params
   * @return {Q.Promise<Reply>}
   */
  setParams(params) {
    var prep = _.isArray(params) ? /* $FlowIgnore: tested with isArray */
      _.map(params, paramPartToXml) : paramPartToXml(params);
    return this.cmd.invoke({ parameters: { data: prep } });
  }
}

module.exports = DicXmlParams;
