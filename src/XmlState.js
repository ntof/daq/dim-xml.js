// @ts-check

module.exports = {
  /** @enum {{ value: number, strValue: string }} */
  States: {
    NotReady: { value: -1, strValue: 'NOT READY' },
    Error: { value: -2, strValue: 'ERROR' }
  }
};
