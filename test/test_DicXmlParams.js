// @ts-check
const
  q = require('q'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),

  { DisXmlNode, DicXmlParams, XmlData } = require('../src');

describe('DicXmlParams', function() {
  var env;

  beforeEach(function() {
    env = {
      dis: new DisXmlNode()
    };
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can use params', async function() {
    var dataset = env.dis.addXmlParams('/pwet');
    dataset.add({ name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
      value: 1234 });
    dataset.add({ name: 'sample2', unit: 'lux', type: XmlData.Type.ENUM,
      value: 0, valueName: 'first value',
      enum: [
        { value: 0, name: 'first value' },
        { value: 1, name: 'second value' }
      ]
    });
    await dataset.synced;

    var client = new DicXmlParams('/pwet', env.dis.info);
    await client
    .then((value) => {
      expect(value).to.have.lengthOf(2);
      expect(value[0]).to.deep.contain({ index: 0, value: 1234 });
    })
    .then(() => {
      var def = q.defer();
      client.once('value', (value) => {
        expect(value).to.have.lengthOf(2);
        expect(value[0]).to.deep.contain({ index: 0, value: 1235 });
        expect(value[1]).to.deep.contain({ index: 1, value: 1, valueName:
          'second value' });
        def.resolve();
      });
      client.setParams([
        { index: 0, value: 1235, type: XmlData.Type.INT64 },
        { index: 1, value: 1, type: XmlData.Type.ENUM }
      ]);
      return def.promise;
    })
    .finally(() => client.release());
  });

  it('can use nested params', async function() {
    var dataset = env.dis.addXmlParams('/pwet');
    dataset.add({ name: 'sample1', unit: 'lux', type: XmlData.Type.INT64,
      value: 1234 });
    dataset.add({ name: 'sample2',
      value: [
        { name: 'nSample1', unit: 'lux', type: XmlData.Type.INT64, value: 1 },
        { name: 'nSample2', unit: 'lux', type: XmlData.Type.INT64, value: 2 }
      ] });
    dataset.add({ name: 'sample3', unit: 'lux', type: XmlData.Type.INT64,
      value: 4321 });
    await dataset.synced;

    var client = new DicXmlParams('/pwet', env.dis.info);
    await client
    .then((value) => {
      expect(value).to.have.lengthOf(3);
      expect(value[0]).to.deep.contain({ index: 0, value: 1234 });
      expect(value[1]).to.deep.contain({ index: 1, value: [
        { index: 0, name: 'nSample1', unit: 'lux',
          type: XmlData.Type.INT64, value: 1 },
        { index: 1, name: 'nSample2', unit: 'lux',
          type: XmlData.Type.INT64, value: 2 }
      ] });
      expect(value[2]).to.deep.contain({ index: 2, value: 4321 });
    })
    .then(() => {
      var def = q.defer();
      client.once('value', (value) => {
        expect(value).to.have.lengthOf(3);
        expect(value[0]).to.deep.contain({ index: 0, value: 1235 });
        expect(value[1].value[0]).to.deep.contain({ index: 0, value: 3 });
        expect(value[1].value[1]).to.deep.contain({ index: 1, value: 4 });
        expect(value[2]).to.deep.contain({ index: 2, value: 5432 });
        def.resolve();
      });
      client.setParams([ // This is wrong, we should receive only one value event
        { index: 5, value: 'wrong', type: XmlData.Type.STRING }
      ]);
      client.setParams([
        { index: 0, value: 1234, type: XmlData.Type.INT64 }, // Test forEach continue
        { index: 0, value: 1235, type: XmlData.Type.INT64 },
        { index: 1, value: [
          { index: 0, value: 3, type: XmlData.Type.INT64 },
          { index: 1, value: 4, type: XmlData.Type.INT64 }
        ] },
        { index: 2, value: 5432, type: XmlData.Type.INT64 }
      ]);
      return def.promise;
    })
    .finally(() => client.release());
  });
});
