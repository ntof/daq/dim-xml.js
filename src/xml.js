// @ts-check
const
  _ = require('lodash'),
  { DOMParser, DOMImplementation, XMLSerializer } = require('xmldom');

/**
 * @typedef {import('./types').XmlJsAttrList} XmlJsAttrList
 * @typedef {import('./types').XmlJsObject} XmlJsObject
 */

const NodeType = {
  ELEMENT_TYPE: 1,
  TEXT_NODE: 3
};

var _implem = new DOMImplementation();

/**
 * @param {string} tag
 */
function createDocument(tag) {
  return _implem.createDocument('', tag, null);
}

const _serializer = new XMLSerializer();

/**
 * @param {Node} node
 */
function serializeToString(node) {
  return _serializer.serializeToString(node);
}

/**
 * @param {Element} node
 * @param {object} attrs
 */
function fromJsAttrs(node, attrs) {
  _.forEach(attrs, function(value, key) {
    node.setAttribute(key, _.toString(value));
  });
}

/**
 * @brief generate xml document from javascript object
 * @param  {Element} node
 * @param {XmlJsObject} args
 * @return {Element}
 */
function fromJs(node, args) {
  if (_.isObjectLike(args)) {
    _.forEach(args, function(value, key) {
      if (key === '$') {
        fromJsAttrs(node, value);
      }
      else if (key === '$textContent') {
        const elt = node.ownerDocument.createTextNode(_.toString(value));
        node.appendChild(elt);
      }
      else if (_.isArray(value)) {
        _.forEach(value, function(value) {
          const elt = node.ownerDocument.createElement(key);
          fromJs(elt, value);
          node.appendChild(elt);
        });
      }
      else {
        const elt = node.ownerDocument.createElement(key);
        fromJs(elt, value);
        node.appendChild(elt);
      }
    });
  }
  else {
    const elt = node.ownerDocument.createTextNode(_.toString(args));
    node.appendChild(elt);
  }
  return node;
}

/**
 * @brief convert xml to json
 * @param  {Element} node
 * @return {XmlJsObject}
 */
function toJs(node) {
  /** @type {XmlJsObject} */
  var ret = {};
  var text = '';

  _.forEach(node.childNodes, function(sub) {
    const element = /** @type {Element} */ (sub);
    if (element.nodeType === NodeType.ELEMENT_TYPE) {
      var parent = _.get(ret, element.tagName);
      if (parent) {
        if (_.isArray(parent)) {
          parent.push(toJs(element));
        }
        else {
          ret[element.tagName] = [ parent, toJs(element) ];
        }
      }
      else {
        ret[element.tagName] = toJs(element);
      }
    }
    else if (sub.nodeType === NodeType.TEXT_NODE) {
      text += /** @type {Text} */ (sub).data;
    }
  });
  if (node.attributes.length !== 0) {
    /** @type {XmlJsAttrList} */
    var attrs = {};
    _.forEach(node.attributes, function(a) { attrs[a.name] = a.value; });
    ret['$'] = attrs;
  }
  if (!_.isEmpty(text)) {
    if (_.isEmpty(ret)) {
      // @ts-ignore
      ret = text;
    }
    else {
      ret['$textContent'] = text;
    }
  }
  return ret;
}

/**
 * @brief get text from an XML element
 * @details iterates on childs to retrieve data from text nodes
 * @param {Element} node
 */
function getText(node) {
  var text = '';
  _.forEach(node.childNodes, function(sub) {
    if (sub.nodeType === NodeType.TEXT_NODE) {
      text += /** @type {Text} */ (sub).data;
    }
  });
  return text;
}

/**
 * @brief get specific child of an XML element
 * @details iterates on children to retrieve node with specific tag
 * @param {Node|Element} node
 * @param {string} tag
 * @return {Array<Element>}
 */
function getChildNodes(node, tag){
  /** @type {Array<Element>} */
  const children = [];
  if (_.isNil(node)) { return children; }
  // eslint-disable-next-line guard-for-in
  for (const child in node.childNodes) {
    const index = _.toNumber(child);
    if (_.isNaN(index)) { continue; }
    if (node.childNodes[index].nodeType === NodeType.ELEMENT_TYPE &&
      /** @type {Element} */ (node.childNodes[index]).tagName === tag) {
      children.push(/** @type {Element} */ (node.childNodes[index]));
    }
  }
  return children;
}

module.exports = {
  DOMParser,
  createDocument,
  serializeToString,
  fromJs,
  toJs,
  getText,
  getChildNodes
};
