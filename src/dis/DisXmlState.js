// @ts-check

const
  prom = require('@cern/prom'),
  _ = require('lodash'),
  debug = require('debug')('dim:xml'),
  { States } = require('../XmlState'),
  xml = require('../xml'),
  { DisService } = require('@cern/dim');

/**
 * @typedef {import('../types').XmlState} XmlState
 * @typedef {import('../types').XmlState.Error} Error
 */

/**
 * @param {Array<Error>} errors
 * @param {string} tag
 * @param {Element} root
 */
function serializeErrors(errors, tag, root) {
  var child = root.ownerDocument.createElement(tag + 's');
  _.forEach(errors, function(state) {
    var elt = root.ownerDocument.createElement(tag);
    elt.setAttribute('code', _.toString(state.code));
    elt.appendChild(root.ownerDocument.createTextNode(state.message));
    child.appendChild(elt);
  });
  root.appendChild(child);
}

/**
 * @param {XmlState} states
 * @return {Element}
 */
function serialize(states) {
  var root = xml.createDocument('state').documentElement;
  if (_.isEmpty(states.errors)) {
    root.setAttribute('value', _.toString(states.value));
    root.setAttribute('strValue', states.strValue);
    root.appendChild(root.ownerDocument.createElement('errors'));
  }
  else {
    root.setAttribute('value', _.toString(States.Error.value));
    root.setAttribute('strValue', States.Error.strValue);
    serializeErrors(states.errors, 'error', root);
  }
  _.forEach(states.description, function(state) {
    var elt = root.ownerDocument.createElement('value');
    elt.setAttribute('value', _.toString(state.value));
    elt.setAttribute('strValue', state.strValue);
    root.appendChild(elt);
  });
  serializeErrors(states.warnings, 'warning', root);
  return root;
}

/**
 * @extends DisService<string>
 */
class DisXmlState extends DisService {

  constructor() {
    super('C');
    /** @type {XmlState} */
    this.states = {
      value: States.NotReady.value,
      strValue: States.NotReady.strValue,
      errors: [],
      warnings: [],
      description: [ States.NotReady, States.Error ]
    };
    this._nextIndex = 0;
  }

  /**
   * @param {number} value
   * @param {string} strValue
   * @return {boolean}
   */
  addState(value, strValue) {
    if (value < 0) {
      debug('can\'t modify builtin states:', value);
      return false;
    }
    else if (_.some(this.states.description, { value })) {
      debug('state already registered:', value);
      return false;
    }
    this.states.description.push({ value, strValue });
    this.states.description = _.sortBy(this.states.description, 'value');
    this._setDirty();
    return true;
  }

  /**
   * @param {number} value
   * @return {boolean}
   */
  removeState(value) {
    var idx = _.findIndex(this.states.description, { value });
    if (idx >= 0) {
      if (this.states.value === this.states.description[idx].value) {
        this.setState(States.NotReady.value);
      }
      this.states.description.splice(idx, 1);
      this._setDirty();
      return true;
    }
    return false;
  }

  /**
   * @param {number} value
   * @return {boolean}
   */
  setState(value) {
    var state = _.find(this.states.description, { value });
    if (!state) {
      debug('unknown state:', value);
      return false;
    }
    this.states.value = state.value;
    this.states.strValue = state.strValue;
    this.states.errors = [];
    this._setDirty();
    return true;
  }

  /**
   * @param {number} code
   * @param {string} message
   */
  addWarning(code, message) {
    var state = _.find(this.states.warnings, { code });
    if (state) {
      state.message = message;
    }
    else {
      this.states.warnings.push({ code, message });
    }
    this._setDirty();
  }

  /**
   * @param {number} code
   * @return {boolean}
   */
  removeWarning(code) {
    var idx = _.findIndex(this.states.warnings, { code });
    if (idx >= 0) {
      this.states.warnings.splice(idx, 1);
      this._setDirty();
      return true;
    }
    return false;
  }

  /**
   * @param {number} code
   * @param {string} message
   */
  addError(code, message) {
    var error = _.find(this.states.errors, { code });
    if (error) {
      error.message = message;
    }
    else {
      this.states.errors.push({ code, message });
    }
    this._setDirty();
  }

  /**
   * @param {number} code
   * @return {boolean}
   */
  removeError(code) {
    var idx = _.findIndex(this.states.errors, { code });
    if (idx >= 0) {
      this.states.errors.splice(idx, 1);
      this._setDirty();
      return true;
    }
    return false;
  }

  _setDirty() {
    if (!this._dirty) {
      this._dirty = true;
      _.defer(this._updateXML.bind(this));
    }
  }

  _updateXML() {
    if (this._dirty) {
      this._dirty = false;
      this.setValue(xml.serializeToString(serialize(this.states)));
    }
  }

  get synced() {
    if (!this._dirty) {
      return Promise.resolve();
    }
    else {
      const ret = prom.makeDeferred();
      this.once('update', () => ret.resolve());
      return prom.timeout(ret, 3000);
    }
  }
}

module.exports = DisXmlState;
